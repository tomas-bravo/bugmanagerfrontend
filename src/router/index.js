// Composables
import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/layouts/noAppBar/NoAppBarLayout.vue'),
    children: [
      {
        path: "",
        name: 'Login',
        component: () => import('@/views/LoginView.vue')
      }
    ]
  },
  {
    path: '/external_app',
    component: () => import('@/layouts/default/Default.vue'),
    children: [
      {
        path: "",
        name: "external_app",
        component: () => import('@/layouts/externa_app/ExternalDefault.vue'),
        children: [
          {
            path: '',
            name: 'appSelector',
            component: () => import("@/views/external_app/AppSelector.vue")
          },
          {
            path: 'login',
            name: "external_app_login",
            component: () => import('@/views/external_app/LoginView.vue')
          },
          {
            path: 'home',
            name: "external_app_home",
            component: () => import('@/views/external_app/HomeView.vue')
          },
        ]
      },
    ],
  },
  {
    path: "/",
    component: () => import('@/layouts/app/AppLayout.vue'),
    children: [
      {
        path: 'tickets/admin',
        name: 'tickets.admin',
        component: () => import('@/views/admin/AdminTicketsShow.vue')
      },
      {
        path: 'tickets/dev',
        name: 'tickets.dev',
        component: () => import('@/views/developer/TicketIndex.vue')
      },
      {
        path: 'user/:id',
        name: 'userInfo',
        component: () => import('@/views/UserInfo.vue')
      },
      {
        path: 'app/:id',
        name: 'appInfo',
        component: () => import('@/views/AppInfo.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
