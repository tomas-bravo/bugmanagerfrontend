import { defineStore } from "pinia"
import { ref } from "vue"
import axios from "axios"

const baseUrl = 'http://localhost:3000/api/'

export const useApiStore = defineStore('apiStore', () => {

    const user = ref();
    const tickets = ref([]);
    const ticket = ref();
    const prioridades = ref([]);
    const devs = ref([])
    const result = ref();
    const software = ref();

    async function getTicketsNuevos() {
        try {
            const json = await axios.get(baseUrl + "tickets/news")
            tickets.value = json.data;
        } catch (error) {
            this.ticket_nuevos = []
            throw error
        }
    }
    async function getTicketsAsignados() {
        try {
            const json = await axios.get(baseUrl + "tickets/asignados")
            tickets.value = json.data;
        } catch (error) {
            tickets.value = []
            result.value = error
            throw error
        }
    }
    async function getTicketsAsignadosDev(devId) {
        try {
            const json = await axios.get(baseUrl + `tickets/asignados?devId=${devId}`)
            tickets.value = json.data;
        } catch (error) {
            tickets.value = []
            result.value = error.response
            throw error
        }
    }
    // async function getTicketsAccepted() {
    //     try {
    //         let json = await axios.get(baseUrl + "tickets/aceptados")
    //         this.ticket_aceptados = json.data;
    //     } catch (error) {
    //         alert(error);
    //         console.log(error)
    //         this.ticket_aceptados = []
    //     }
    // }
    async function getTicketById(id) {
        try {
            const json = await axios.get(baseUrl + `tickets?ticketId=${id}`)
            ticket.value = json.data;
        } catch (error) {
            ticket.value = {}
            result.value = error.response
            throw error
        }
    }
    async function getAllPrioridades() {
        try {
            const json = await axios.get(baseUrl + "prioridades")
            this.prioridades = json.data
        } catch (error) {
            prioridades.value = []
            throw error
        }
    }
    async function getAppDevs(id) {
        try {
            const json = await axios.get(baseUrl + `usuarios?softwareId=${id}`)
            devs.value = json.data
        } catch (error) {
            devs.value = []
            throw error
        }
    }
    async function asignarTicket(devId, pId) {
        const json = await axios.patch(baseUrl + "tickets/proponer", {
            ticketId: ticket.value.id,
            devId: devId,
            prioridadId: pId
        })
        result.value = json
    }
    async function aceptarTicket(devId, ticketId) {
        const json = await axios.patch(baseUrl + 'tickets/asigne', {
            id: ticketId,
            developerId: devId
        })
        result.value = json
    }
    async function reasignarTicket(id, devId) {
        const json = await axios.post(baseUrl + "tickets/reasignar", {
            ticketId: id,
            devId: devId
        })
        result.value = json
    }
    async function finalizarTicket(ticketId, userId) {
        const json = await axios.patch(baseUrl + "tickets/finalizar", {
            ticketId: ticketId, usuarioId: userId,
        })
        result.value - json
    }
    async function getAppInfo(id) {
        try {
            const json = await axios.get(baseUrl + `apps?softwareId=${id}`)
            software.value = json.data
        } catch (error) {
            software.value = null
            throw error
        }
    }
    async function getUser(id) {
        try {
            const json = await axios.get(baseUrl + `usuarios?devId=${id}`)
            user.value = json.data
        } catch (error) {
            user.value = null
            throw error
        }

    }
    return {
        // variables
        user,
        tickets,
        ticket,
        prioridades,
        devs,
        result,
        software,
        // funciones
        getTicketsNuevos,
        getTicketsAsignados,
        getTicketById,
        getAllPrioridades,
        getAppDevs,
        getAppInfo,
        getUser,
        asignarTicket,
        aceptarTicket,
        reasignarTicket,
        finalizarTicket,
        getTicketsAsignadosDev
    }
})