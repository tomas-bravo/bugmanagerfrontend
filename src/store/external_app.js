import { defineStore } from "pinia";
import { computed, ref } from "vue";
import axios from "axios";


const baseUrl = 'http://localhost:3000/api/'

export const useExternalStore = defineStore('externalStore', () => {
    const username = ref('');
    const useremail = ref('');
    const app_name = ref('');
    const descripcion = ref("");
    const pasos = ref('');
    const titulo = ref('')
    const images = ref([]);
    const isLogin = ref(false)
    const apps = ref([])
    const fetchApps = async () => {
        try {
            const json = await axios.get(baseUrl + 'apps')
            apps.value = json.data
        } catch (error) {
            alert(error)
            apps.value = []
        }
    }
    const submitTicket = async () => {
        try {
            const appId = apps.value.filter((e) => e.nombre === app_name.value)[0].id
            console.log(appId)
            let data = new FormData()
            for (const file of images.value)
                data.append("files", file)
            data.append("titulo", titulo.value)
            data.append("nombre_reportero", username.value)
            data.append("correo_reportero", useremail.value)
            data.append("app_id", `${appId}`)
            data.append("estado_id", "1")
            data.append("detalles", descripcion.value)
            data.append("pasos", pasos.value)
            const result = await axios.post(baseUrl + "tickets/create", data,
                {
                    headers: {
                        "Content-Type": "multipart/form-data",
                    }
                })
            return result
        } catch (error) {
            console.log(error)
            return error
        }
    }
    const getAppNames = computed(() => {
        return apps.value.map((e) => e.nombre)
    })
    return {
        useremail,
        username,
        titulo,
        descripcion,
        pasos,
        images,
        app_name,
        isLogin,
        getAppNames,
        fetchApps,
        submitTicket
    };
}
)