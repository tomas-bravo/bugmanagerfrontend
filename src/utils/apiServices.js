import { useApiStore } from "@/store/apiStore"
import { storeToRefs } from "pinia"


export default () => {
    const store = useApiStore()
    const {
        user,
        tickets,
        ticket,
        prioridades,
        devs,
        result,
        software
    } = storeToRefs(store)

    const getTicketsNuevos = async () => {
        try {
            await store.getTicketsNuevos()
        } catch (error) {
            console.log(error)
            throw error
        }
    }
    const getTicketsAsignados = async () => {
        try {
            await store.getTicketsAsignados()
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getTicketsAsignadosDev = async (devId) => {
        try {
            await store.getTicketsAsignadosDev(devId)
        } catch (error) {
            console.log(error)
            throw error
        }
    }
    const getTicket = async (id) => {
        try {
            await store.getTicketById(id)
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getPrioridades = async () => {
        try {
            await store.getAllPrioridades()
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getAppDevs = async (id) => {
        try {
            await store.getAppDevs(id)
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getApp = async (id) => {
        try {
            await store.getAppInfo(id)
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const getUser = async (id) => {
        try {
            await store.getUser(id)
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const asignarTicket = async (pId, devId) => {
        try {
            await store.asignarTicket(devId, pId)
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const aceptarTicket = async (devId, ticketId) => {
        try {
            await store.aceptarTicket(devId, ticketId)
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    const reasignarTicket = async (ticketId, devId) => {
        try {
            await store.reasignarTicket(ticketId, devId)
        } catch (error) {
            console.log(error)
            throw error
        }
    }
    const finalizarTicket = async (ticketId, devId) => {
        try {
            await store.finalizarTicket(ticketId, devId)
        } catch (error) {
            console.log(error)
            throw error
        }
    }

    return {
        //variables
        user,
        tickets,
        ticket,
        prioridades,
        devs,
        result,
        software,
        // funciones
        getTicketsNuevos,
        getTicketsAsignados,
        getTicket,
        getPrioridades,
        getAppDevs,
        getApp,
        getUser,
        asignarTicket,
        aceptarTicket,
        reasignarTicket,
        finalizarTicket,
        getTicketsAsignadosDev
    }
}